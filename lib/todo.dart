import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:dart_composable_architecture/composable_architecture/reducer.dart';
import 'package:dart_composable_architecture/providers.dart';

import 'composable_architecture/utils.dart';
import 'main.dart';

part 'todo.freezed.dart';

class TodoWidget extends StatefulWidget {
  final int index;
  final Todo todo;

  TodoWidget({required this.todo, required this.index}) : super(key: ValueKey(todo.id));

  @override
  State<StatefulWidget> createState() => TodoState();
}

class TodoState extends State<TodoWidget> {
  late TextEditingController _controller;
  late String _currentText;

  void _sendAction(TodoAction action) {
    context.read(stateNotifierProvider.notifier).send(
      AppAction.todoAction(widget.index, action)
    );
  }

  @override
  void initState() {
    super.initState();
    _currentText = widget.todo.description;
    _controller = TextEditingController(text: _currentText);
    _controller.addListener(() {
      if (_controller.text != _currentText) {
        _currentText = _controller.text;
        _sendAction(TodoAction.textFieldChanged(_currentText));
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Checkbox(
              value: widget.todo.isCompleted,
              onChanged: (_) {
                _sendAction(TodoAction.checkboxTapped());
              }),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _controller,
            ),
          ),
        ),
      ],
    );
  }
}

@freezed
class Todo with _$Todo {
  const factory Todo({required String id, @Default("") String description, @Default(false) bool isCompleted}) = _Todo;
}

@freezed
class TodoAction with _$TodoAction {  
  const factory TodoAction.checkboxTapped() = TodoCheckboxTapped;
  const factory TodoAction.textFieldChanged(String text) = TodoTextFieldChanged;
}

abstract class TodoEnvironment {
}

Reducer<Todo, TodoAction, TodoEnvironment> todoReducer = Reducer(
  (todo, action, environment) => action.when(
    checkboxTapped: () => Pair(todo.copyWith(isCompleted: !todo.isCompleted), []), 
    textFieldChanged: (text) => Pair(todo.copyWith(description: text), [])
  )
);