import 'package:dart_composable_architecture/main.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';
import 'todo.dart';

import 'composable_architecture/store.dart';

final stateNotifierProvider = StateNotifierProvider<Store<AppState, AppAction, AppEnvironment>, AppState>(
  (ref) => Store(
    environment: AppEnvironment(Uuid()),
    initialState: AppState(todos: [Todo(id: "first", description: "First todo"), Todo(id: "second", description: "Second todo")]),
    reducer: appReducer,
    debug: true
  )
);
