import 'package:dart_composable_architecture/composable_architecture/reducer.dart';
import 'package:dart_composable_architecture/composable_architecture/utils.dart';
import 'package:dart_composable_architecture/providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'composable_architecture/effect.dart';
import 'todo.dart';
import 'package:uuid/uuid.dart';

part 'main.freezed.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProviderScope(
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends ConsumerWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  Widget build(BuildContext context, ScopedReader watch) {
    final state = watch(stateNotifierProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView(
          children: state.todos
              .mapIndexed((index, todo) => TodoWidget(
                    index: index,
                    todo: todo,
                  ))
              .toList()),
      floatingActionButton: FloatingActionButton(
        onPressed: () => context.read(stateNotifierProvider.notifier).send(AppAction.addButtonTapped()),
        tooltip: 'Create new goal',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

@freezed
class AppState with _$AppState {
  const factory AppState({@Default([]) List<Todo> todos}) = _AppState;
}

@freezed
class AppAction with _$AppAction {
  const factory AppAction.todoAction(int index, TodoAction action) = AppTodoAction;
  const factory AppAction.addButtonTapped() = AddButtonTapped;
  const factory AppAction.checkboxDelayCompleted() = CheckboxDelayCompleted;
}

class AppEnvironment implements TodoEnvironment {
  final Uuid uuid;
  AppEnvironment(this.uuid);
}

Reducer<AppState, AppAction, AppEnvironment> appReducer = combineReducers([
  toGlobalReducer<AppState, AppAction, AppEnvironment, Todo, TodoAction, TodoEnvironment>(
    todoReducer, 
    (appState, appAction) => (appAction is AppTodoAction) ? appState.todos[appAction.index] : null, 
    (appAction) => (appAction is AppTodoAction) ? appAction.action : null, 
    (appEnv) => appEnv,
    (previousState, Todo todo, appAction) => (appAction is AppTodoAction) ? previousState.copyWith(
      todos: previousState.todos.mapIndexed((index, td) => (index == appAction.index) ? todo : td).toList()
    ) : null
  ),
  Reducer(
    (state, action, env) => action.when(
      addButtonTapped: () => Pair(state.copyWith(todos: state.todos + [Todo(id: env.uuid.v1())]), []),
      checkboxDelayCompleted: () => Pair(state.copyWith(todos: moveCompletedToEnd(state.todos)), []),
      todoAction: (index, action) {
        if (action is TodoCheckboxTapped) {
          return Pair(state, [Effect(
            name: "checkboxDelay",
            run: (cancelEffects) {
              cancelEffects("checkboxDelay");
              return Future.delayed(const Duration(seconds: 1)).then((_) => AppAction.checkboxDelayCompleted());
            }
          )]);
        }
      }
    )
  )
]);

List<Todo> moveCompletedToEnd(List<Todo> input) {
  return input.where((todo) => !todo.isCompleted).toList()
    ..addAll(input.where((todo) => todo.isCompleted));
}