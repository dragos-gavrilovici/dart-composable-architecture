import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:dart_composable_architecture/composable_architecture/reducer.dart';
import 'effect.dart';

class Store<S, A, E> extends StateNotifier<S> {

  final Reducer<S, A, E> _reducer;
  final E environment;
  final _debug;

  final Map<int, _RunningEffect> _runningEffects = {};
  int _nextRunningEffect = 0;

  Store({
    required Reducer<S, A, E> reducer, 
    required S initialState, 
    required this.environment,
    bool debug = false}) :
      _reducer = reducer,
      _debug = debug,
      super(initialState);

  void send(A action) {
    if (_debug) {
      print("Received action: $action");
    }
    final result = _reducer.reducerFunction(state, action, environment);
    if (result != null) {
      state = result.first;
      result.second.forEach(_runEffect);
      if (_debug) {
        print("New state: $state");
      }
    }
  }

  void _runEffect(Effect<A> effect) {
    int runningId = _nextRunningEffect++;
    _runningEffects[runningId] = _RunningEffect(effect.name);
    effect.run(_createCancelEffectsFunction(runningId)).then((action) {
      if (action != null && !_runningEffects[runningId]!._cancelled) {
        send(action);
      }
      _runningEffects.remove(runningId);
    });
  }

  CancelEffects _createCancelEffectsFunction(int runningId) {
    return (name) {
      for (final entry in _runningEffects.entries) {
        if (entry.key != runningId && entry.value._name == name) {
          entry.value._cancelled = true;
        }
      }
    };
  }

}

class _RunningEffect {
  final String _name;
  bool _cancelled = false;
  _RunningEffect(this._name);
}
