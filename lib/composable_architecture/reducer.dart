import 'package:dart_composable_architecture/composable_architecture/effect.dart';
import 'package:dart_composable_architecture/composable_architecture/utils.dart';

class Reducer<S, A, E> {
  final Pair<S, List<Effect<A>>>? Function(S, A, E) reducerFunction;

  Reducer(this.reducerFunction);
}

Reducer<GS, GA, GE> toGlobalReducer<GS, GA, GE, LS, LA, LE>(
  Reducer<LS, LA, LE> localReducer,
  LS? Function(GS, GA) stateGlobalToLocal, 
  LA? Function(GA) actionGlobalToLocal,
  LE Function(GE) envGlobalToLocal,
  GS? Function(GS, LS, GA) stateLocalToGlobal
) {
  return Reducer<GS, GA, GE>(
    (globalState, globalAction, globalEnv) {
      final localState = stateGlobalToLocal(globalState, globalAction);
      final localAction = actionGlobalToLocal(globalAction);
      LS? newLocalState;
      if (localState != null && localAction != null) {
        newLocalState = localReducer.reducerFunction(
          localState,
          localAction,
          envGlobalToLocal(globalEnv)
        )?.first;
      }
      if (newLocalState != null) {
        final newGlobalState = stateLocalToGlobal(globalState, newLocalState, globalAction);
        if (newGlobalState != null) {
          return Pair(newGlobalState, []);
        }
      }
      return null;
    }
  );
}

Reducer<S, A, E> combineReducers<S, A, E>(List<Reducer<S, A, E>> reducers) => Reducer(
  (state, action, env) {
    S tempState = state;
    List<Effect<A>> effects = [];
    for (final reducer in reducers) {
      final result = reducer.reducerFunction(tempState, action, env);
      if (result != null) {
        tempState = result.first;
        effects.addAll(result.second);
      }
    }
    return Pair(tempState, effects);
  }
);

