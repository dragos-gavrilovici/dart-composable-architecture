class Effect<A> {
  final String name;
  final Future<A?> Function(CancelEffects) run;

  Effect({required this.name, required this.run});
}

typedef CancelEffects = void Function(String);
