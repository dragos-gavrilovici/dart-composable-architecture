import 'package:freezed_annotation/freezed_annotation.dart';

part 'utils.freezed.dart';

@freezed
class Pair<F, S> with _$Pair<F, S> {
  const factory Pair(F first, S second) = _Pair;
}

@freezed
class Triplet<F, S, T> with _$Triplet<F, S, T> {
  const factory Triplet(F first, S second, T third) = _Triplet;
}
